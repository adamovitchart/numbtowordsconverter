package main.java.com.adamo;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by adamo on 12.01.2018.
 */
public class NumbToWordsConverter {


    // инициализация начальных данных
    private boolean FLAG_DATA_INIT = false; //флаг чтения данных из файла
    private final String PATH_FILE_TRIADS = "data/triadNames.txt"; // путь к файлу, содержащему названия различных триад
    private final String PATH_FILE_UNITS = "data/unitNames.txt"; // путь к файлу, содержащему названия чисел, полученных возведением тысячи в степеь
    private final static String SEPARATOR = " ";
    private final static int MALE_GENDER = 1; // необходим для выборки один/одна или два/две
    private final static int FEMALE_GENDER = -1;
    private Map<Integer, String> triadNames;  //ключ — число
    private Map<Integer, String> unitNames;  //ключ — степень тысячи
    private final String[][] endings = {{"а", "и", ""},{"", "а", "ов"}}; //окончания для тысяч (тысяч_а, тысяч_и, тысяча_), миллионов (миллион, миллион_а, миллион_ов) и т.д.

    public String convertNumbToWords (BigInteger numbToConvert) throws IOException {
        //инициализация мапов
        readDataFromFile();
        String numberToWorkWith = numbToConvert.toString();
        String result = "";

        //убираем в начале минус и добавляем слово "минус" к результату
        if (numberToWorkWith.charAt(0) == '-') {
            result += "минус ";
            numberToWorkWith = numberToWorkWith.substring(1);
        }

        // если наше число лежит в интервале от -20 до 20, то мы сразу можем вывести ответ
        try {
            if (Integer.parseInt(numberToWorkWith) <= 20) {
                return result + triadNames.get(Integer.parseInt(numberToWorkWith));
            }
        } catch (NumberFormatException nfe) {}

        // добавляем нули в начало строки, чтобы длина нашей строки была кратна трём (для удобного разбиения на триады и работы с ними)
        for (int i = 0; i < numberToWorkWith.length()%3; i++) {
            numberToWorkWith = "0" + numberToWorkWith;
        }

        try {
            for (int i = 0; i < numberToWorkWith.length() / 3; i++) {
                int degree = numberToWorkWith.length() / 3 - 1 - i;
                if (degree == 1) {
                    result += triadToWords(numberToWorkWith.substring((i * 3), i * 3 + 3), FEMALE_GENDER) + getUnitInWords(degree, numberToWorkWith.substring((i * 3), i * 3 + 3));
                } else {
                    result += triadToWords(numberToWorkWith.substring((i * 3), i * 3 + 3), MALE_GENDER) + getUnitInWords(degree, numberToWorkWith.substring((i * 3), i * 3 + 3));
                }
            }
        } catch (NullPointerException npe) {
            throw new IllegalArgumentException(npe);
        }


        return result.trim();
    }

    private Map<Integer, String> fillMapFromFile (String path) throws IOException {
        Map<Integer, String > mapToFill = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF8"))) {
            String tmpStr;
            String Units[];
            while ((tmpStr = br.readLine()) != null) {
                if (!Objects.equals(tmpStr, "")) {
                    Units = tmpStr.split(SEPARATOR);
                    mapToFill.put(Integer.valueOf(Units[0]), Units[1]);
                }
            }
        } catch (IOException io) {
            throw io;
        }
        return mapToFill;
    }

    private void readDataFromFile() throws IOException {
        while (!FLAG_DATA_INIT) {
            triadNames = fillMapFromFile(PATH_FILE_TRIADS);
            unitNames = fillMapFromFile(PATH_FILE_UNITS);
            FLAG_DATA_INIT = true;
        }
    }

    private String triadToWords (String triad, int gender) {
        String triadInWords = "";
        int amountOfHundreds = Character.getNumericValue(triad.charAt(0));
        int amountOfTens = Character.getNumericValue(triad.charAt(1));
        int amountOfUnits = Character.getNumericValue(triad.charAt(2));

        if (amountOfHundreds != 0) {
            triadInWords += triadNames.get(amountOfHundreds*100) + SEPARATOR;
        }
        if (amountOfTens == 1 && amountOfUnits >= 0 && amountOfUnits <= 9) {
            triadInWords += triadNames.get(10 + amountOfUnits) + SEPARATOR;
        }
        else {
            if (amountOfTens != 0) {
                triadInWords += triadNames.get(amountOfTens*10) + SEPARATOR;
            }
            if (amountOfUnits != 0) {
                if(amountOfUnits <= 2) {
                    triadInWords += triadNames.get(amountOfUnits * gender) + SEPARATOR;
                }
                else {
                    triadInWords += triadNames.get(amountOfUnits) + SEPARATOR;
                }
            }
        }

        return triadInWords;
    }

    // выбираем название с уже выбранным окончанием, основанным на числе
    private String getUnitInWords (int degree, String number) {
        if (unitNames.get(degree) == null && degree != 0)
            throw new NullPointerException("Don't exist such degree of thousand");
        if (Objects.equals(number, "000")) {
            return "";
        }
        if (degree == 1) {
            return unitNames.get(degree) + endings[0][getNumberOfEnding(Integer.parseInt(number))] + SEPARATOR;
        } else {

        }
        if (degree > 1) {
            return unitNames.get(degree) + endings[1][getNumberOfEnding(Integer.parseInt(number))] + SEPARATOR;
        }
        return "";
    }

    // по последним двум цифрам выбираем номер окончания из заданного в начале массива
    private int getNumberOfEnding (int n) {
        int lastTwoDigits = n % 100;
        int lastDigit = n % 10;
        if (lastTwoDigits >=5 && lastTwoDigits <= 20) return 2;
        if (lastDigit >= 2 && lastDigit <=4) return 1;
        if (lastDigit == 1) return 0;
        return 2;
    }

}
