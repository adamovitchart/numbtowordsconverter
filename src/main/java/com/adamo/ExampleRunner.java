package main.java.com.adamo;

import java.io.IOException;
import java.math.BigInteger;

/**
 * Created by adamo on 12.01.2018.
 */
public class ExampleRunner {
    public static void main(String[] args) throws IOException {
        BigInteger valuesToConvert[] = {new BigInteger("123456789101112131415161718192021222324252627282930"),
                                        new BigInteger("000000021"),
                                        new BigInteger(String.valueOf(Integer.MAX_VALUE)),
                                        new BigInteger(String.valueOf(Long.MIN_VALUE)),
                                        new BigInteger("-1999000888123098758822")};
        NumbToWordsConverter numbToWordsConverter = new NumbToWordsConverter();
        for (int i = 0; i<valuesToConvert.length; i++) {
            try {
                System.out.println(numbToWordsConverter.convertNumbToWords(valuesToConvert[i]));
            } catch (IOException ioe) {
                System.out.println("Something went wrong.");
            }
        }
//        System.out.println(numbToWordsConverter.convertNumbToWords(new BigInteger("121001")));
    }
}
