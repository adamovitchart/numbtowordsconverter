package main.java.com.adamo.test;

import main.java.com.adamo.NumbToWordsConverter;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

/**
 * Created by adamo on 13.01.2018.
 */
public class NumbToWordsConverterTest {
    NumbToWordsConverter converter = new NumbToWordsConverter();

    public final String resultsOfCheck0_19[] = {"один", "три", "пять", "семь", "девять", "одиннадцать",
                                                "тринадцать", "пятнадцать", "минус один", "минус два",
                                                "минус четыре", "минус девятнадцать", "минус семь", "десять" };
    public final int numbersToCheck0_19[] = {1, 3, 5, 7, 9, 11, 13, 15, -1, -2, -4, -19, -7, 10};
    public final String resultOfCheck20_999[] = {"двадцать один", "сорок семь", "двадцать восемь", "сто сорок восемь",
                                                "сто восемьдесят восемь", "сорок пять", "девятьсот девяносто один",
                                                "семьсот сорок шесть", "пятьсот два", "шестьсот", "четыреста семь",
                                                "четыреста четыре", "триста два", "сто"};
    public final int numbersToCheck20_999[] = {21, 47, 28, 148, 188, 45, 991, 746, 502, 600, 407, 404, 302, 100};
    public final String resultOfCheck[] = {"два миллиарда сто сорок семь миллионов четыреста восемьдесят три тысячи шестьсот сорок шесть",
                                            "один миллион одна тысяча", "два миллиона две тысячи", "сто сорок семь тысяч",
                                            "восемь миллионов семьсот пятьдесят восемь тысяч восемьсот двадцать два"};
    public final int numbersToCheck[] = {2147483646, 1001000, 2002000, 147000, 8758822};


    @Test
    public void testOneToWords () throws IOException {
        System.out.println("JUnit test 1");
        String ONE = "один";
        assertEquals("Something went wrong", ONE, converter.convertNumbToWords(BigInteger.ONE));
        System.out.println("JUnit test 1 : success");
    }

    @Test
    public void testNumbersToWords0_19 () throws IOException {
        System.out.println("JUnit test 0 — 19");
        for (int i = 0; i<numbersToCheck0_19.length; i++) {
            assertEquals("Error in number: " + numbersToCheck0_19[i], resultsOfCheck0_19[i],
                            converter.convertNumbToWords(new BigInteger(String.valueOf(numbersToCheck0_19[i]))));
        }
        System.out.println("JUnit test 0 — 19: success");
    }

    @Test
    public void testNumbersToWords20_999 () throws IOException {
        System.out.println("JUnit test 20 — 999");

        for (int i = 0; i<numbersToCheck0_19.length; i++) {
            assertEquals("Error in number: " + numbersToCheck20_999[i], resultOfCheck20_999[i],
                    converter.convertNumbToWords(new BigInteger(String.valueOf(numbersToCheck20_999[i]))));
        }
        System.out.println("JUnit test 20 — 999: success");
    }

    @Test
    public void testNumbersToWords() throws IOException {
        System.out.println("JUnit test big numbers");

        for (int i = 0; i<numbersToCheck.length; i++) {
            assertEquals("Error in big value: " + numbersToCheck[i], resultOfCheck[i],
                    converter.convertNumbToWords(new BigInteger(String.valueOf(numbersToCheck[i]))));
        }
        System.out.println("JUnit test big numbers: success");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNumberOverLimit () throws IOException {
        try {
            System.out.println("JUnit test number over limit");
            converter.convertNumbToWords(new BigInteger("999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999"));
        } catch (IllegalArgumentException iae) {
            System.out.println("JUnit test number over limit: success");
            throw new IllegalArgumentException(iae);
        }
    }

    @Test
    public void testNumberToWordsDDT () throws IOException {
        System.out.println("JUnit test Data-Driven test using data from excel file");

        InputStream inputStream = new FileInputStream("testData/test.xls");
        Workbook wb = new HSSFWorkbook(inputStream);

        BigInteger numbToCheck = new BigInteger("0");
        String resultString = null;
        Sheet sheet = wb.getSheetAt(0);
        DataFormatter formatter = new DataFormatter();
        for (int i = 1; i <= sheet.getLastRowNum(); i++) {
            for (int j = 0; j < sheet.getRow(i).getLastCellNum(); j++) {
                switch (j) {
                    case 0:
                        String valueOfCell = formatter.formatCellValue(sheet.getRow(i).getCell(j));
                        numbToCheck = new BigInteger(valueOfCell);
                        break;
                    case 1:
                        resultString = sheet.getRow(i).getCell(j).getStringCellValue();
                        break;
                    default:
                        break;
                }
            }
            assertEquals("Error in number: " + numbToCheck, resultString,
                    converter.convertNumbToWords(numbToCheck));
        }
        System.out.println("JUnit test Data-Driven test using data from excel file: success");
    }

}
